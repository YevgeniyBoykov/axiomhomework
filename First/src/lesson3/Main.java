package lesson3;

import java.util.Scanner;

public class Main
{
    private static final String[] menu = {"1. Fill array from test-array.",
                                            "2. Add string to array.",
                                            "3. Get string by index.",
                                            "4. Delete string from array by index.",
                                            "5. Delete string from array by value.",
                                            "6. Print array.",
                                            "7. Exit."};
    public static void main(String[] args)
    {
        Test arrayForTest = new Test();
        HipOfString hipArrObject = new HipOfString();

        boolean err = false;
        do {
            for (int i = 0; i < 7; i++) {
                System.out.println(menu[i]);
            }
            Scanner scan = new Scanner(System.in);
            int menuItem = scan.nextInt();

            if (menuItem < 1 || menuItem > 7){
                System.out.println("Enter a number 1-7");
            }

            switch (menuItem){
                case 1:
                    for(int i = 0; i < arrayForTest.arrayForTest.length; i++){
                        hipArrObject.add(arrayForTest.arrayForTest[i]);
                    }
                    break;
                case 2:
                    System.out.println("Enter string of array for add: ");
                    Scanner scanAdd = new Scanner(System.in);
                    String strToAdd = scanAdd.nextLine();
                    hipArrObject.add(strToAdd);
                    break;
                case 3:
                    System.out.println("Enter index of array to get: ");
                    int indexToGet = scan.nextInt();
                    if(hipArrObject.get(indexToGet) == null)
                        System.out.println("Nothing find!");
                    else
                        System.out.println(hipArrObject.get(indexToGet));
                    break;
                case 4:
                    System.out.println("Enter index of array to del: ");
                    int indexToDel = scan.nextInt();
                    hipArrObject.removeByIndex(indexToDel-1);
                    break;
                case 5:
                    System.out.println("Enter value of array to del: ");
                    Scanner scanDel = new Scanner(System.in);
                    String strToDel = scanDel.nextLine();
                    hipArrObject.removeByValue(strToDel);
                    break;
                case 6:
                    hipArrObject.prnArr();
                    break;
                case 7:
                    err = true;
                    break;
            }
        } while (!err);
    }
}
