package lesson3;

import java.util.Scanner;

public class HipOfString
{
    private String[] hipString = new String[3];
    private String[] hipBuffer;

    public int size(String[] mass){
        return mass.length;
    }
    //the method, which created new element of array
    public void add(String newStr){
        int index = getNumberPosition();
        //if fill the las free element of array
        if (index == size(hipString)){
            //copy array to array buffer
            hipBuffer = new String[size(hipString)];
            for(int i=0; i< size(hipString); i++){
                hipBuffer[i] = hipString[i];
            }
            //recreate Array
            hipString = new String[size(hipString)*2];
            //copy buffer array to new array
            for(int i=0; i< size(hipBuffer); i++){
                hipString[i] = hipBuffer[i];
            }
            hipString[index] = newStr;
        }
        hipString[index] = newStr;
        prnArr();
    }

    // method "print the array"
    void prnArr (){
        if(getNumberPosition() ==0)
            System.out.println("Is Blank 0");
        else {
            for (int i = 0; i < getNumberPosition(); i++) {
                System.out.print(hipString[i] + " ;");
            }
        }
        System.out.println(" ИЗ " + size(hipString));
    }
    // this method return last "no-null" position in array
    int getNumberPosition(){
        int index = size(hipString);
        for (int i = size(hipString)-1; i >= 0; i--){
            if (hipString[i] == null){
                index = i;
            }
        }
        return index;
    }
    // method which return element of array by index
    String get(int index){
        String resultString="";
        for (int i = 0; i < size(hipString); i++){
            if (i == index-1) {
                resultString = hipString[i];
                break;
            }
        }
        return resultString;
    }
    //method remove element by index if found
    void removeByIndex (int index){
        boolean indexFind = false;
        int positionNum = 0;
        if(index==-1)
        {
            index = 0;
        }
        if(index < 0){
            System.out.println("Введите правильное значение.");
        }
        else {
            for (int i = 0; i < size(hipString); i++) {
                if (i == index) {
                    indexFind = true;
                    positionNum = i;
                }
            }
            if (indexFind) {
                for (int j = positionNum; j <= getNumberPosition(); j++) {
                    hipString[j] = hipString[j + 1];
                }
                hipString[getNumberPosition()] = null;
                System.out.println("Element " + index + " was deleted :-)");
            } else {
                System.out.println("Element with index " + index + " was not found :-(");
            }
        }
        prnArr();
    }
    //method remove element by value if found
    void removeByValue (String removeString){
        boolean strFind = false;
        int positionNum = 0;
        for(int i = 0; i < size(hipString); i++){
            if(removeString.equals(hipString[i])){
                strFind = true;
                positionNum = i;
            }
        }
        if (strFind){
            for(int j = positionNum; j <= getNumberPosition(); j++){
                hipString[j] = hipString[j+1];
            }
            hipString[getNumberPosition()] = null;
            System.out.println("Element " + positionNum + " was deleted :-)");
        }
        else {
            System.out.println("Element with value \"" + removeString + "\" was not found :-(");
        }
        prnArr();
    }

}
