package task8;

import task7.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    private static Class ClassAnnotations;

    public static void main(String[] args) {
        ClassAnnotations testClass = new ClassAnnotations();
        runner(testClass.getClass());
    }

    public static void runner(Class classAnnotation) {
        int countFinding = 0;
        for (Method methodList : classAnnotation.getDeclaredMethods()){
            if(methodList.isAnnotationPresent(Runner.class)){
                try {
                    methodList.invoke(null);
                    countFinding++;
                } catch (InvocationTargetException | IllegalAccessException wrappedExc) {
                    Throwable ups = wrappedExc.getCause();
                    System.out.println(methodList + " failed: " + ups);
                }
            }
        }
        System.out.printf("Found " + countFinding + " methods!");
    }
}
