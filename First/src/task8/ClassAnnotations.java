package task8;

public class ClassAnnotations {
    @Runner
    public static void first(){
        System.out.println("This is first method!");
    }
    public static void second(){
        System.out.println("This is second method!");
    }
    @Runner
    public static void third(){
        System.out.println("This is third method!");
    }
}
