package lesson4;

public abstract class GeomFigura {
    private static String name;
    public abstract double getArea();
    public abstract double getPerimeter();
    public static String getName() {
        return name;
    }
}
