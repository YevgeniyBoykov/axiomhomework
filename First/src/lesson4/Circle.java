package lesson4;

import static java.lang.Math.PI;
import static java.lang.Math.*;

public class Circle extends GeomFigura{
    private static String name = "Circle";
    private float radius;

    public Circle(float radius) {
        if(radius>0)
            this.radius = radius;
        else throw new IllegalArgumentException("Radius can't be 0 or less");
    }

    @Override
    public double getArea() {
        return PI*pow(radius, 2);
    }

    @Override
    public double getPerimeter() {
        return PI*radius;
    }

    public static String getName() {
        return name;
    }
}
