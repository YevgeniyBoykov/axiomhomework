package lesson4;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Trapezium extends GeomFigura {

    private static String name = "Trapezium";
    private float topSide, bottomSide, height;

    public Trapezium(float topSide, float bottomSide, float height) {
        if(topSide > 0 && bottomSide > 0 && height > 0){
            this.topSide = topSide;
            this.bottomSide = bottomSide;
            this.height = height;
        }
        else throw new NumberFormatException("All value must be more 0.");

    }

    @Override
    public double getArea() {
        return (double) (((topSide + bottomSide)*height) / 2);
    }

    @Override
    public double getPerimeter() {
        double sideCD = sqrt(pow((bottomSide - topSide)/2, 2) + pow(height, 2));
        return topSide + bottomSide + sideCD*2;
    }

    public static String getName() {
        return name;
    }
}
