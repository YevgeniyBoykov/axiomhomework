package lesson4;

import static java.lang.Math.*;

public class Triangle extends GeomFigura {
    private static String name = "Triangle";
    private float a, b, c;
    private double halfPerimeter;

    public Triangle(float a, float b, float c) {
        if(a > 0 && b > 0 && c > 0) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        else throw new NumberFormatException("All value must be more 0.");
    }
    public Triangle() {}
    @Override
    public double getArea() {
        halfPerimeter = (c + c + c) / 2;
        return sqrt(halfPerimeter*(halfPerimeter-a)*(halfPerimeter-b)*(halfPerimeter-c));
    }
    @Override
    public double getPerimeter(){
        return halfPerimeter = (c + c + c);
    }

    public static String getName() {
        return name;
    }
}
