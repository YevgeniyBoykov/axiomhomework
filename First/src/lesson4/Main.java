package lesson4;

import java.util.Scanner;

public class Main {

    public static void getResult(GeomFigura shape, String method){
        if (method.equals("Square")) {
            System.out.println("Square " + shape.getName() + " is " + shape.getArea());
        } else if (method.equals("Perimeter")) {
            System.out.println("Square " + shape.getName() + " is " + shape.getPerimeter());
        }
    }

    public static void main(String[] args) {
        String[] arrFigure = new String[] {
                Circle.getName(),
                Triangle.getName(),
                Square.getName(),
                Rectangle.getName(),
                Trapezium.getName()
        };


        for (int i = 0; i < arrFigure.length; i++) {
           System.out.println(arrFigure[i]);
        }
        System.out.println("Enter Figure: ");
        Scanner scanFig = new Scanner(System.in);
        String menuItem = scanFig.nextLine();

        System.out.println("Square or Perimeter?");
        Scanner scanFigVal = new Scanner(System.in);
        String value = scanFigVal.nextLine();

        switch (menuItem)
        {
            case "Circle":
            {
                System.out.println("Enter radius:");
                Scanner scanCircle = new Scanner(System.in);
                int circleRadius = scanCircle.nextInt();

                Circle myCircle = new Circle(circleRadius);
                getResult(myCircle, value);
                break;
            }
            case "Triangle":
            {
                Scanner scanTriangle = new Scanner(System.in);
                System.out.println("Enter side A:");
                int triangleA = scanTriangle.nextInt();
                System.out.println("Enter side B:");
                int triangleB = scanTriangle.nextInt();
                System.out.println("Enter side C:");
                int triangleC = scanTriangle.nextInt();

                Triangle myTriangle = new Triangle(triangleA, triangleB, triangleC);
                getResult(myTriangle, value);
                break;
            }
            case "Square":
            {
                System.out.println("Enter side:");
                Scanner scanSquare = new Scanner(System.in);
                int squareSideA = scanSquare.nextInt();

                Square mySquare = new Square(squareSideA);
                getResult(mySquare, value);
                break;
            }
            case "Rectangle":
            {
                System.out.println("Enter side A:");
                Scanner scanRectangleA = new Scanner(System.in);
                int rectangleSideA = scanRectangleA.nextInt();
                System.out.println("Enter side B:");
                Scanner scanRectangleB = new Scanner(System.in);
                int rectangleSideB = scanRectangleB.nextInt();

                Rectangle myRectangle = new Rectangle(rectangleSideA, rectangleSideB);
                getResult(myRectangle, value);
                break;
            }
            case "Trapezium":
            {
                System.out.println("Enter side A:");
                Scanner scanTrapeziumA = new Scanner(System.in);
                int trapeziumSideA = scanTrapeziumA.nextInt();
                System.out.println("Enter side B:");
                Scanner scanTrapeziumB = new Scanner(System.in);
                int trapeziumSideB = scanTrapeziumB.nextInt();
                System.out.println("Enter heigth:");
                Scanner scanTrapeziumHeight = new Scanner(System.in);
                int trapeziumHeight = scanTrapeziumHeight.nextInt();

                Trapezium myTrapezium = new Trapezium(trapeziumSideA, trapeziumSideB, trapeziumHeight);
                getResult(myTrapezium, value);
                break;
            }
            default:
                System.out.println("Wrong!");
        }
    }

}

