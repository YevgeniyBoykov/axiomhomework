package lesson4;

public class Square extends GeomFigura{
    private static String name = "Square";
    private float side;


    public Square(float side) {
        if(side > 0)
            this.side = side;
        else throw new NumberFormatException("Sidevalue for square must be more 0.");
    }

    @Override
    public double getArea() {
        return side * side;
    }

    @Override
    public double getPerimeter() {
        return side * 4;
    }

    public static String getName() {
        return name;
    }
}
