package lesson4;

import java.util.IllegalFormatException;

public class Rectangle extends GeomFigura {
    private static String name = "Rectangle";
    private float a, b;


    public Rectangle(float a, float b) {
        if(a > 0 && b > 0) {
            this.a = a;
            this.b = b;
        }
        else throw new IllegalArgumentException("All value must be more 0.");
    }

    @Override
    public double getArea() {
        return a * b;
    }

    @Override
    public double getPerimeter() {
        return 2 * a + 2 * b;
    }

    public static String getName() {
        return name;
    }
}
