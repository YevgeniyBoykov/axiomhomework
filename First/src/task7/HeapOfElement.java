package task7;

import java.util.Iterator;

public class HeapOfElement<T> implements Iterable {

    private T[] myHeap = (T[]) new Object[3];
    private T[] myHeapBuffer;

    public int size(T[] mass) {
        return mass.length;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int index;
            @Override
            public boolean hasNext() {
                return index != myHeap.length;
            }
            @Override
            public T next() {
                return myHeap[index++];
            }
        };
    };
    //the method, which created new element of array
    public void add(T newElement) {
        int index = getNumberPosition();
        //if fill the las free element of array
        if (index == size(myHeap)) {
            //copy array to array buffer
            myHeapBuffer = (T[]) new Object[size(myHeap)];
            for (int i = 0; i < size(myHeap); i++) {
                myHeapBuffer[i] = myHeap[i];
            }
            //recreate Array
            myHeap = (T[]) new Object[size(myHeap) * 2];
            //copy buffer array to new array
            for (int i = 0; i < size(myHeapBuffer); i++) {
                myHeap[i] = myHeapBuffer[i];
            }
            myHeap[index] = newElement;
        }
        myHeap[index] = newElement;
        prnArr();
    }

    // method "print the array"
    void prnArr() {
        if (getNumberPosition() == 0)
            System.out.println("Is Blank 0");
        else {
            for (int i = 0; i < getNumberPosition(); i++) {
                System.out.print(myHeap[i] + "; ");
            }
        }
        System.out.println(" ИЗ " + size(myHeap));
    }

    // this method back last notnull position in array
    int getNumberPosition() {
        int index = size(myHeap);
        for (int i = size(myHeap) - 1; i >= 0; i--) {
            if (myHeap[i] == null) {
                index = i;
            }
        }
        return index;
    }

    // method which return element of array by index
    T get(int index) {
        T resultString = null;
        if (index == -1) {
            index = 0;
        }
        if (index < 0) {
            System.out.println("Введите правильное значение.");
        } else {
            for (int i = 0; i < size(myHeap); i++) {
                if (i == index) {
                    resultString = myHeap[i];
                    break;
                }
            }
        }
        return resultString;
    }

    //method remove element by index if found
    void removeByIndex(int index) {
        boolean indexFind = false;
        int positionNum = 0;
        if (index == -1) {
            index = 0;
        }
        if (index < 0) {
            System.out.println("Введите правильное значение.");
        } else {
            for (int i = 0; i < size(myHeap); i++) {
                if (i == index) {
                    indexFind = true;
                    positionNum = i;
                }
            }
            if (indexFind) {
                for (int j = positionNum; j <= getNumberPosition(); j++) {
                    myHeap[j] = myHeap[j + 1];
                }
                myHeap[getNumberPosition()] = null;
                System.out.println("Element " + (index + 1) + " was deleted :-)");
            } else {
                System.out.println("Element with index " + (index + 1) + " was not found :-(");
            }
        }
        prnArr();
    }

    //method remove element by value if found
    void removeByValue(T removeString) {
        boolean strFind = false;
        int positionNum = 0;
        for (int i = 0; i < size(myHeap); i++) {
             if (removeString.equals(myHeap[i])) {
                strFind = true;
                positionNum = i;
            }
        }
        if (strFind) {
            for (int j = positionNum; j <= getNumberPosition(); j++) {
                myHeap[j] = myHeap[j + 1];
            }
            myHeap[getNumberPosition()] = null;
            System.out.println("Element " + (positionNum+1) + " was deleted :-)");
        } else {
            System.out.println("Element with value \"" + removeString + "\" was not found :-(");
        }
        prnArr();
    }
}
